//
//  RiderViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Anthony Benitez on 12/7/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse
import MapKit

class RiderViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var requestButton: UIButton!
   
    var locationManager = CLLocationManager()
    
    var riderRequestActive = false
    var userLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    var driverOnTheWay = false
    
    func displayAlert(title: String, message: String) {
        let alertcontroller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertcontroller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertcontroller, animated: true, completion: nil)
    }
    
    // overrides status bar color to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func requestOrCancelUber(_ sender: Any) {
        
        // if request is active, and button is pressed
        if riderRequestActive == true {
            // set buttonntitle and cancel request
            requestButton.setTitle("Request Uber", for: [])
            riderRequestActive = false
            
            // query to get the Request and delete it
            let query = PFQuery(className: "RiderRequest")
            query.whereKey("username", equalTo: (PFUser.current()?.username)!)
            query.findObjectsInBackground(block: { (objects, error) in
                    
                if let riderRequests = objects {
                    for riderRequest in riderRequests {
                            riderRequest.deleteInBackground()
                        }
                    }
                })
            // if request if not active and button is pressed
            } else if riderRequestActive == false {
                // if user location is set (rare chance to be exactly at 0, 0)
                if userLocation.latitude != 0 && userLocation.longitude != 0 {
                    
                    // activate request and set button title
                    riderRequestActive = true
                    self.requestButton.setTitle("Cancel Uber", for: [])
                    let riderRequest = PFObject(className: "RiderRequest")
                    
                    riderRequest["username"] = PFUser.current()?.username
                    riderRequest["location"] = PFGeoPoint(latitude: userLocation.latitude, longitude: userLocation.longitude)
                    riderRequest.saveInBackground(block: { (success, error) in
                        
                        if success {
                            print("Called an uber")
                        } else {
                            
                            self.requestButton.setTitle("Call An Uber", for: [])
                            self.riderRequestActive = false
                            self.displayAlert(title: "Could not call Uber", message: "Please try again!")
                        }
                    })
                    
                } else {
                    displayAlert(title: "Could not call Uber", message: "Cannot detect your location.")
                }
                
            }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logoutFromRider" {
            locationManager.stopUpdatingLocation()
            PFUser.logOut()
        }
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set locationManager settings
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        // hides button to not show until loading process is done
        requestButton.isHidden = true
        
        let query = PFQuery(className: "RiderRequest")
        query.whereKey("username", equalTo: (PFUser.current()?.username)!)
        query.findObjectsInBackground(block: { (objects, error) in
            
            if let objects = objects {
                if objects.count > 0 {
                    self.riderRequestActive = true
                    self.requestButton.setTitle("Cancel Uber", for: [])
                }
            }
            
            self.requestButton.isHidden = false
        })
    }
    
    // runs constantly to update current location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // alternate way to get the location, this time directly from the manager
        if let location = manager.location?.coordinate {
            userLocation = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            
            if driverOnTheWay == false {
                let region = MKCoordinateRegion(center: userLocation, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                self.map.setRegion(region, animated: true)
                
                // remove all annotations from the map
                self.map.removeAnnotations(map.annotations)
            
                // add annotation to current location
                self.map.removeAnnotations(self.map.annotations)
                let annotation = MKPointAnnotation()
                annotation.coordinate = userLocation
                annotation.title = "Your current location"
                self.map.addAnnotation(annotation)
            }
            
            
            // query to insert location to the current user
            let query = PFQuery(className: "RiderRequest")
            query.whereKey("username", equalTo: PFUser.current()?.username)
            query.findObjectsInBackground(block: { (objects, error) in
                
                if let riderRequests = objects {
                    for riderRequest in riderRequests {
                        
                        riderRequest["location"] = PFGeoPoint(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                        riderRequest.saveInBackground()
                    }
                }
            })
        }
        
        if riderRequestActive == true {
            let query = PFQuery(className: "RiderRequest")
            query.whereKey("username", equalTo: PFUser.current()?.username)
            query.findObjectsInBackground(block: { (objects, error) in
                if let riderRequests = objects {
                    for riderRequest in riderRequests {
                        if let driverUsername = riderRequest["driverResponded"] {
                    
                            let query = PFQuery(className: "DriverLocation")
                            query.whereKey("username", equalTo: driverUsername)
                            query.findObjectsInBackground(block: { (objects, error) in
                                if let driverLocations = objects {
                                    for driverLocationObject in driverLocations {
                                        if let driverLocation = driverLocationObject["location"] as? PFGeoPoint {
                                            
                                            self.driverOnTheWay = true
                                            
                                            let driverCLLocation = CLLocation(latitude: driverLocation.latitude, longitude: driverLocation.longitude)
                                            let riderCLLocation = CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
                                            
                                            let distance = riderCLLocation.distance(from: driverCLLocation) / 1000
                                            let roundedDistance = round(distance * 100) / 100
                                            
                                            self.requestButton.setTitle("Your driver is \(roundedDistance)Km away!", for: [])
                                            
                                            let latDelta = abs(driverLocation.latitude - self.userLocation.latitude) * 2 + 0.05
                                            let lonDelta = abs(driverLocation.longitude - self.userLocation.longitude) * 2 + 0.05
                                            let region = MKCoordinateRegion(center: self.userLocation, span: MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: lonDelta))
                                            self.map.setRegion(region, animated: true)
                                            
                                            let userLocationAnnotation = MKPointAnnotation()
                                            userLocationAnnotation.coordinate = self.userLocation
                                            userLocationAnnotation.title = "Your Location"
                                            self.map.addAnnotation(userLocationAnnotation)
                                            
                                            let driverLocationAnnotation = MKPointAnnotation()
                                            userLocationAnnotation.coordinate = CLLocationCoordinate2D(latitude: driverLocation.latitude, longitude: driverLocation.longitude)
                                            userLocationAnnotation.title = "Driver: \(driverUsername)"
                                            self.map.addAnnotation(driverLocationAnnotation)
                                        }
                                    }
                                }
                                
                            })
                        }
                    }
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
