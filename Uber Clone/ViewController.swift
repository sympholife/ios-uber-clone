//
//  ViewController.swift
//  Uber Clone
//
//  Created by Anthony Benitez on 12/5/16.
//  Copyright © 2016 SymphoLife. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {
    
    var loginOrSignup = "Login"
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var riderOrDriverSwitch: UISwitch!
    @IBOutlet weak var submitSignupOrLoginButton: UIButton!
    @IBOutlet weak var haveAnAccountLabel: UILabel!
    @IBOutlet weak var driverLabel: UILabel!
    @IBOutlet weak var riderLabel: UILabel!
    @IBOutlet weak var changeRegistrationMethodButton: UIButton!
    
    func displayAlert(title: String, message: String) {
        let alertcontroller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertcontroller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alertcontroller, animated: true, completion: nil)
    }
    
    @IBAction func changeRegistrationMethod(_ sender: Any) {
        if loginOrSignup == "Login" {
            loginOrSignup = "Signup"
            
            submitSignupOrLoginButton.setTitle("Sign Up", for: [])
            changeRegistrationMethodButton.setTitle("Login", for: [])
            haveAnAccountLabel.text = "Have an account already?"
            
            // show driverOrRider options
            driverLabel.isHidden = false
            riderLabel.isHidden = false
            riderOrDriverSwitch.isHidden = false
        } else if loginOrSignup == "Signup" {
            loginOrSignup = "Login"
            
            submitSignupOrLoginButton.setTitle("Login", for: [])
            changeRegistrationMethodButton.setTitle("Sign Up", for: [])
            haveAnAccountLabel.text = "Don't have an account yet?"
            
            // hide driverOrRider options
            driverLabel.isHidden = true
            riderLabel.isHidden = true
            riderOrDriverSwitch.isHidden = true
        }
    }
    
    
    @IBAction func signUpOrLogin(_ sender: Any) {
        if usernameTextField.text == "" || passwordTextField.text == "" {
            
            displayAlert(title: "Error in form", message: "Username and password are required")
            
        } else {
            
            if loginOrSignup == "Signup" {
                
                let user = PFUser()
                user.username = usernameTextField.text
                user.password = passwordTextField.text
                if riderOrDriverSwitch.isOn {
                    user["user_type"] = "rider"
                } else {
                    user["user_type"] = "driver"
                }
                
                user.signUpInBackground(block: { (success, error) in
                    
                    if let error = error {
                        var displayedErrorMessage = "Please try again later"
                        let error = error as NSError
                        
                        if let parseError = error.userInfo["error"] as? String {
                            displayedErrorMessage = parseError
                        }
                        
                        self.displayAlert(title: "Sign Up Failed", message: displayedErrorMessage)
                    } else {
                        print("Sign Up Successful")
                        
                        if let userType = PFUser.current()?["user_type"] as? String {
                            if userType == "driver" {
                                self.performSegue(withIdentifier: "showDriverViewController", sender: self)
                            } else if userType == "rider" {
                                self.performSegue(withIdentifier: "showRiderViewController", sender: self)
                            }
                        }
                    }
                })
            } else if loginOrSignup == "Login" {
                PFUser.logInWithUsername(inBackground: usernameTextField.text!, password: passwordTextField.text!, block: { (user, error) in
                    
                    if let error = error {
                        var displayedErrorMessage = "Please try again later"
                        let error = error as NSError
                        
                        if let parseError = error.userInfo["error"] as? String {
                            displayedErrorMessage = parseError
                        }
                        
                        self.displayAlert(title: "Sign Up Failed", message: displayedErrorMessage)
                        
                    } else {
                        print("Log In Successful")
                        
                        if let userType = PFUser.current()?["user_type"] as? String {
                            if userType == "driver" {
                                self.performSegue(withIdentifier: "showDriverViewController", sender: self)
                            } else if userType == "rider" {
                                self.performSegue(withIdentifier: "showRiderViewController", sender: self)
                            }
                        }
                    }
                })
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let userType = PFUser.current()?["user_type"] as? String {
            if userType == "rider" {
                self.performSegue(withIdentifier: "showRiderViewController", sender: self)
            } else if userType == "driver" {
                self.performSegue(withIdentifier: "showDriverViewController", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userSpacerView = UIView(frame:CGRect(x:0, y:0, width:50, height:10))
        let passSpacerView = UIView(frame:CGRect(x:0, y:0, width:50, height:10))
        usernameTextField.leftViewMode = UITextFieldViewMode.always
        usernameTextField.leftView = userSpacerView
        passwordTextField.leftViewMode = UITextFieldViewMode.always
        passwordTextField.leftView = passSpacerView
        
        // Sets border-radius to button
        submitSignupOrLoginButton.layer.cornerRadius = 5
        submitSignupOrLoginButton.clipsToBounds = true;
        
        // Because app turns on as Login for default, hide the driver or rider switch to show it when registration is chaned
        driverLabel.isHidden = true
        riderLabel.isHidden = true
        riderOrDriverSwitch.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

